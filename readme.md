# Weatherstack API Client

Weatherstack is a service that allows developers to retrieve past, current and
forecasted weather information for specific locations.

This project is a Composer-friendly PHP library with the goal of making it easy
to interact with weatherstack's API.

## Installation

To install this package, simply bring it in via composer:

```bash
$ composer require craftemy/weatherstack
```

## Response Information

The responses from this client are the same as the responses from weatherstack,
including the response structure. For more information, refer to weatherstack's
API documentation:

> https://weatherstack.com/documentation

## Usage

There are a number of options that can be provided to use with this client:

```php
<?php

use Craftemy\Weatherstack\Client;

$client = new Client([
    // The API key provided by weatherstack
    'apiKey' => 'example',

    // An hourly interval to use for historical/forecast weather
    'interval' => 3,

    // The unit of measurement to return results as: Celsius, Fahrenheit,
    // or Scientific
    'units' => 'f',

    // The language to return string results
    'language' => 'en',

    // An optional JSONP callback function
    'callback' => 'handleResponse',

    // Whether or not to use an HTTPS connection with the weatherstack
    // endpoints. This option requires a paid weatherstack plan.
    'useSSL' => true,
]);

// The above options can also be set by calling their related setter method
$client
    ->withMetric()
    ->withSSL(true)
    ->withApiKey('second_example');
    
// There are a number of helpers for setting location information; the following
// would individually set the query parameter to look for Schenectady, NY, USA.
$client->withPostalCode('12345')
    ->withLocation('Schenectady')
    ->withCoordinates(42.817467, -73.91653);

// Fetch the current weather for the currently-set location
$client->getCurrentWeather();

// Fetch historical weather information from January 1, 2020
$client->getHistoricalWeather('2020-01-01');

// Fetch historical weather information from January 1, February 1, and March 1
// 2020
$client->getHistoricalWeather('2020-01-01', '2020-02-01', '2020-03-01');
// or
$client->getHistoricalWeather(...['2020-01-01', '2020-02-01', '2020-03-01']);

// Fetch weather information for every day between the given two dates
// (inclusive)
$client->getHistoricalTimeSeries('2020-01-01', '2020-02-01');

// Fetch the weather forecast
$client->getWeatherForecast(7);

// Fetch a list of potential location matches
$client->withLocation('miami')->getAutocompletionInformation();
```

## Bug Reports

If you see an issue with how the library works, please feel free to file an issue!

> https://gitlab.com/craftemy/weatherstack-sdk/-/issues