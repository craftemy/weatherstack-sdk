<?php

namespace Craftemy\Weatherstack;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class Client
{
    private ?string $apiKey = null;
    private ?string $query = null;
    private ?int $hourly = null;
    private ?int $interval = null;
    private ?string $units = null;
    private ?string $language = null;
    private ?string $callback = null;
    private bool $useSSL = true;
    protected ?GuzzleClient $client = null;

    private bool $clientSecure = true;


    /**
     * Creates a new weatherstack API client
     *
     * @param array<string, mixed> $config An associative array with known configuration keys and values
     */
    public function __construct(array $config = [])
    {
        $knownConfig = [
            'apiKey' => 'withApiKey',
            'query' => 'withQuery',
            'interval' => 'withInterval',
            'units' => 'withUnits',
            'language' => 'withLanguage',
            'callback' => 'withCallback',
            'useSSL' => 'withSSL',
        ];
        foreach ($knownConfig as $key => $function) {
            if (isset($config[$key])) {
                $this->$function($config[$key]);
            }
        }
    }

    /**
     * This returns a Guzzle client that can be interacted with for raw weatherstack calls
     *
     * @return GuzzleClient An HTTP client
     */
    public function getClient(): GuzzleClient
    {
        if ($this->client === null || $this->useSSL !== $this->clientSecure) {
            $this->client = new GuzzleClient([
                'base_uri' => $this->getApiBaseUri(),
                'http_errors' => false,
            ]);
            $this->clientSecure = $this->useSSL;
        }
        return $this->client;
    }

    /**
     * This function returns the URL of the weatherstack API service
     *
     * @return string A string representing the API URI
     */
    private function getApiBaseUri(): string
    {
        return ($this->useSSL ? 'https' : 'http') . '://api.weatherstack.com';
    }

    /**
     * This endpoint fetches the current weather for the configured location
     *
     * @return mixed The API response from weatherstack
     * @throws WeatherstackException
     */
    public function getCurrentWeather()
    {
        try {
            $response = json_decode(
                $this->getClient()->request('GET', '/current', [
                    'query' => array_filter([
                        'access_key' => $this->apiKey,
                        'query' => $this->query,
                        'units' => $this->units,
                        'language' => $this->language,
                        'callback' => $this->callback,
                    ])
                ])->getBody(),
                true,
                16,
                JSON_THROW_ON_ERROR
            );

            if (isset($response['error'])) {
                throw new WeatherstackException($response['error']['info']);
            }

            return $response;
        } catch (GuzzleException | JsonException $e) {
            throw new WeatherstackException('The response from the Weatherstack service was not properly formed.');
        }
    }

    /**
     * Retrieves weather information for a date in the past
     *
     * @param string ...$date The date or dates requested, each as its own parameter. To provide an array of dates, use
     *                        "..." to unpack the array (e.g. `$client->getHistoricalWeather(...$array)`)
     * @return mixed The API response from weatherstack
     * @throws WeatherstackException
     */
    public function getHistoricalWeather(string ...$date)
    {
        try {
            $response = json_decode(
                $this->getClient()->request('GET', '/historical', [
                    'query' => array_filter([
                        'access_key' => $this->apiKey,
                        'query' => $this->query,
                        'historical_date' => implode(';', $date),
                        'hourly' => $this->hourly,
                        'interval' => $this->interval,
                        'units' => $this->units,
                        'language' => $this->language,
                        'callback' => $this->callback,
                    ])
                ])->getBody(),
                true,
                16,
                JSON_THROW_ON_ERROR
            );

            if (isset($response['error'])) {
                throw new WeatherstackException($response['error']['info']);
            }

            return $response;
        } catch (GuzzleException | JsonException $e) {
            throw new WeatherstackException('The response from the Weatherstack service was not properly formed.');
        }
    }

    /**
     * Retrieves the historical weather for a location over the period of specified days (max 60 days)
     *
     * @param string $date_start The first date
     * @param string $date_end The last date of the time series
     * @return mixed The API response from weatherstack
     * @throws WeatherstackException
     */
    public function getHistoricalTimeSeries(string $date_start, string $date_end)
    {
        try {
            $response = json_decode(
                $this->getClient()->request('GET', '/historical', [
                    'query' => array_filter([
                        'access_key' => $this->apiKey,
                        'query' => $this->query,
                        'historical_date_start' => $date_start,
                        'historical_date_end' => $date_end,
                        'hourly' => $this->hourly,
                        'interval' => $this->interval,
                        'units' => $this->units,
                        'language' => $this->language,
                        'callback' => $this->callback,
                    ])
                ])->getBody(),
                true,
                16,
                JSON_THROW_ON_ERROR
            );

            if (isset($response['error'])) {
                throw new WeatherstackException($response['error']['info']);
            }

            return $response;
        } catch (GuzzleException | JsonException $e) {
            throw new WeatherstackException('The response from the Weatherstack service was not properly formed.');
        }
    }

    /**
     * Retrieves the weather forecast for the given location up to the specified days in the future
     *
     * @param int $days The number of requested forecast days, between 1 and 14
     * @return mixed The API response from weatherstack
     * @throws WeatherstackException
     */
    public function getWeatherForecast(int $days = 7)
    {
        try {
            $response = json_decode(
                $this->getClient()->request('GET', '/forecast', [
                    'query' => array_filter([
                        'access_key' => $this->apiKey,
                        'query' => $this->query,
                        'forecast_days' => $days,
                        'hourly' => $this->hourly,
                        'interval' => $this->interval,
                        'units' => $this->units,
                        'language' => $this->language,
                        'callback' => $this->callback,
                    ])
                ])->getBody(),
                true,
                16,
                JSON_THROW_ON_ERROR
            );

            if (isset($response['error'])) {
                throw new WeatherstackException($response['error']['info']);
            }

            return $response;
        } catch (GuzzleException | JsonException $e) {
            throw new WeatherstackException('The response from the Weatherstack service was not properly formed.');
        }
    }

    /**
     * Retrieves possible query matches for the specified location to help clear up ambiguous locations
     * (e.g. London could mean Canada or UK)
     *
     * @return mixed The API response from weatherstack
     * @throws WeatherstackException
     */
    public function getAutocompletionInformation()
    {
        try {
            $response = json_decode(
                $this->getClient()->request('GET', '/autocomplete', [
                    'query' => array_filter([
                        'access_key' => $this->apiKey,
                        'query' => $this->query,
                        'callback' => $this->callback,
                    ])
                ])->getBody(),
                true,
                16,
                JSON_THROW_ON_ERROR
            );

            if (isset($response['error'])) {
                throw new WeatherstackException($response['error']['info']);
            }

            return $response;
        } catch (GuzzleException | JsonException $e) {
            throw new WeatherstackException('The response from the Weatherstack service was not properly formed.');
        }
    }

    /**
     * Sets the API key for this API client
     *
     * @param string $value The API key
     * @return Client
     */
    public function withApiKey(string $value): Client
    {
        $this->apiKey = $value;
        return $this;
    }

    /**
     * Sets the hourly interval to the provided value. If set as null, will retrieve the daily summary. Note that
     * weatherstack currently accepts 1, 3, 6, 12 and 24 as valid intervals; an invalid interval will be set to the
     * default 3 hour interval.
     *
     * @param int|null $value
     * @return Client
     */
    public function withInterval(?int $value): Client
    {
        if ($value === null) {
            $this->hourly = 0;
            $this->interval = 3;
        } else {
            $this->hourly = 1;
            switch ($value) {
                case 1:
                case 3:
                case 6:
                case 12:
                case 24:
                    $this->interval = $value;
                    break;
                default:
                    $this->interval = 3;
            }
        }
        return $this;
    }

    /**
     * Sets the location query for this request.
     *
     * @param string $value
     * @return Client
     */
    public function withQuery(string $value): Client
    {
        $this->query = $value;
        return $this;
    }

    /**
     * Sets the location query using a place name (e.g. Singapore). Multiple parameters can be provided to query for
     * more locations.
     *
     * @param string ...$value
     * @return Client
     */
    public function withLocation(string ...$value): Client
    {
        return $this->withQuery(implode(';', $value));
    }

    /**
     * Sets the location query using an array of place names (e.g. ['Singapore', 'Calgary'])
     * @param string[] $values
     * @return Client
     */
    public function withLocations(array $values): Client
    {
        return $this->withQuery(implode(';', $values));
    }

    /**
     * Sets the location query using a US, UK or Canadian postal code.
     *
     * @param string ...$value
     * @return Client
     */
    public function withPostalCode(string ...$value): Client
    {
        return $this->withQuery(implode(';', $value));
    }

    /**
     * Sets the location query using US, UK or Canadian postal codes.
     *
     * @param string[] $values
     * @return Client
     */
    public function withPostalCodes(array $values): Client
    {
        return $this->withQuery(implode(';', $values));
    }

    /**
     * Sets the location query for the given latitude/longitude coordinates
     *
     * @param float $latitude
     * @param float $longitude
     * @return Client
     */
    public function withCoordinates(float $latitude, float $longitude): Client
    {
        return $this->withQuery($latitude . ',' . $longitude);
    }

    /**
     * Retrieves the weather for the given IP address; specifying the default value of "fetch:ip" or providing a null
     * value will use the PHP caller's IP address for the location
     *
     * @param string|null $value
     * @return Client
     */
    public function withIpAddress(?string $value = 'fetch:ip'): Client
    {
        if ($value === null) {
            $value = 'fetch:ip';
        }
        return $this->withQuery($value);
    }

    /**
     * Specifies the units for which information will be measured as: metric/m, fahrenheit/f, or scientific/s
     *
     * @param string $value
     * @return Client
     */
    public function withUnits(string $value): Client
    {
        switch (strtolower($value)) {
            case 'metric':
            case 'm':
                $this->units = 'm';
                break;
            case 'scientific':
            case 's':
                $this->units = 's';
                break;
            case 'fahrenheit':
            case 'f':
                $this->units = 'f';
                break;
            default:
                break;
        }
        return $this;
    }

    /**
     * Sets the units for this request to metric.
     *
     * @return Client
     */
    public function withMetric(): Client
    {
        return $this->withUnits('m');
    }

    /**
     * Sets the units for this request to scientific.
     *
     * @return Client
     */
    public function withScientific(): Client
    {
        return $this->withUnits('s');
    }

    /**
     * Sets the units for this request to fahrenheit.
     *
     * @return Client
     */
    public function withFahrenheit(): Client
    {
        return $this->withUnits('f');
    }

    /**
     * Sets the language for text strings provided in responses
     *
     * @param string $value
     * @return Client
     */
    public function withLanguage(string $value = 'en'): Client
    {
        $this->language = $value;
        return $this;
    }

    /**
     * Sets a JSONP callback function to be provided in the request
     *
     * @param string $value
     * @return Client
     */
    public function withCallback(string $value): Client
    {
        $this->callback = $value;
        return $this;
    }

    /**
     * Specifies whether or not to use SSL encryption when communicating with weatherstack (available for Standard Plan
     * and higher).
     *
     * @param bool $value
     * @return Client
     */
    public function withSSL(bool $value): Client
    {
        $this->useSSL = $value;
        return $this;
    }
}
